﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AnotherModel.Models;

namespace AnotherModel.Controllers
{
    public class HomeController : Controller
    {
        public List<string> Index()
        {
            Model mod = new Model()
            {
                FirstName = "James",
                LastName = "Beck",
                Address = "101 Cameron Road",
                AreaCode = "3122", 
                City = "Tauranga",
            };

            List<string> model = new List<string>
            {
                mod.FirstName,
                mod.LastName,
                mod.Address,
                mod.AreaCode,
                mod.City,
            };

            return model;
        }
    }
}
